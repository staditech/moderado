<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
    public function home() {
        $result = $this->insta();
        return view('home', compact('result'));
    }

    private function insta()
    {
        // Supply a user id and an access token
        $userid = "4989711523";
        $accessToken = "4989711523.44ed748.345200deeba0477da0c372f3e39a0bd2";

        //$userid = "1626494859";
        //$accessToken = "1626494859.44ed748.f1323b782acb4062a2a3d315baf32847";

        // Gets our data
        function fetchData($url){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }

        // Pulls and parses data.
        $result = fetchData("https://api.instagram.com/v1/users/{$userid}/media/recent/?access_token={$accessToken}");
        $result = json_decode($result);

        return $result;
    }
}