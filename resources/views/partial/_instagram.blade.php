<section class="instagram">

    <!-- === instagram header === -->

    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h2 class="h2 title">Follow us <i class="fa fa-instagram fa-2x"></i> Instagram </h2>
                    <div class="text">
                        <p>@InstaFurnitureFactory</p>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- === instagram gallery === -->
    <div class="gallery clearfix">
        @for($i = 0; $i < 6; $i++)
        <?php $post = $result->data[$i]; ?>
        <a class="item" href="{{ $post->link }}" target="_blank">
            <img src="{{ $post->images->standard_resolution->url }}" alt="{{ $post->caption->text }}" />
        </a>
        @endfor
    </div> <!--/gallery-->

</section>