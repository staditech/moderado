<nav class="navbar-fixed">
    <div class="container">
        <!-- ==========  Top navigation ========== -->
        @include('partial.navigation._top')

        <!-- ==========  Main navigation ========== -->
        @include('partial.navigation._menu')
    </div>
</nav>