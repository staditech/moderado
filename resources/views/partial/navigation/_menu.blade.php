<div class="navigation navigation-main">
    <a href="index.html" class="logo"><img src="{{ url('images/logo.svg') }}" alt="MODERADO DESIGN" /></a>
    <a href="#" class="open-login"><i class="icon icon-user"></i></a>
    <a href="#" class="open-search"><i class="icon icon-magnifier"></i></a>
    <a href="#" class="open-cart"><i class="icon icon-cart"></i> <span>3</span></a>
    <a href="#" class="open-menu"><i class="icon icon-menu"></i></a>
    <div class="floating-menu">
        <!--mobile toggle menu trigger-->
        <div class="close-menu-wrapper">
            <span class="close-menu"><i class="icon icon-cross"></i></span>
        </div>
        <ul>
            <li>
                <a href="index.html">Homepage <span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                <div class="navbar-dropdown">
                    <div class="navbar-box">
                        <div class="box-1">
                            <div class="box">
                                <div class="h2">Find your inspiration</div>
                                <div class="clearfix">
                                    <p>Homes that differ in terms of style, concept and architectural solutions have been furnished by Furniture Factory. These spaces tell of an international lifestyle that expresses modernity, research and a creative spirit.</p>
                                    <a class="btn btn-clean btn-big" href="products-grid.html">Shop now</a>
                                </div>
                            </div>
                        </div>
                        <div class="box-2">
                            <div class="box clearfix">
                                <div class="row">
                                    <div class="col-md-4">
                                        <ul>
                                            <li class="label">Home pages</li>
                                            <li><a href="index.html">Home - Slider</a></li>
                                            <li><a href="index-2.html">Home - Tabsy</a></li>
                                            <li><a href="index-3.html">Home - Slider full screen</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <ul>
                                            <li class="label">Product page</li>
                                            <li><a href="products-grid.html">Products grid</a></li>
                                            <li><a href="products-list.html">Products list</a></li>
                                            <li><a href="category.html">Products intro</a></li>
                                            <li><a href="products-topbar.html">Products topbar</a></li>
                                            <li><a href="product.html">Product overview</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <ul>
                                            <li class="label">Blog</li>
                                            <li><a href="blog-grid.html">Blog grid</a></li>
                                            <li><a href="blog-list.html">Blog list</a></li>
                                            <li><a href="blog-grid-fullpage.html">Blog fullpage</a></li>
                                            <li><a href="ideas.html">Blog ideas</a></li>
                                            <li><a href="article.html">Article</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <a href="#">Pages <span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                <div class="navbar-dropdown navbar-dropdown-single">
                    <div class="navbar-box">
                        <div class="box-2">
                            <div class="box clearfix">
                                <ul>
                                    <li class="label">Single dropdown</li>
                                    <li><a href="about.html">About us</a></li>
                                    <li><a href="contact.html">Contact</a></li>
                                    <li><a href="checkout-1.html">Checkout - Cart items</a></li>
                                    <li><a href="checkout-2.html">Checkout - Delivery</a></li>
                                    <li><a href="checkout-3.html">Checkout - Payment</a></li>
                                    <li><a href="checkout-4.html">Checkout - Receipt</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <a href="category.html">Icons <span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                <div class="navbar-dropdown">
                    <div class="navbar-box">
                        <div class="box-1">
                            <div class="image">
                                <img src="assets/images/blog-2.jpg" alt="Lorem ipsum" />
                            </div>
                            <div class="box">
                                <div class="h2">Best ideas</div>
                                <div class="clearfix">
                                    <p>Homes that differ in terms of style, concept and architectural solutions have been furnished by Furniture Factory. These spaces tell of an international lifestyle that expresses modernity, research and a creative spirit.</p>
                                    <a class="btn btn-clean btn-big" href="ideas.html">Explore</a>
                                </div>
                            </div>
                        </div>
                        <div class="box-2">
                            <div class="clearfix categories">
                                <div class="row">
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-sofa"></i>
                                                <figcaption>Sofa</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-armchair"></i>
                                                <figcaption>Armchairs</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-chair"></i>
                                                <figcaption>Chairs</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-dining-table"></i>
                                                <figcaption>Dining tables</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-media-cabinet"></i>
                                                <figcaption>Media storage</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-table"></i>
                                                <figcaption>Tables</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-bookcase"></i>
                                                <figcaption>Bookcase</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-bedroom"></i>
                                                <figcaption>Bedroom</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-nightstand"></i>
                                                <figcaption>Nightstand</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-children-room"></i>
                                                <figcaption>Children room</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-kitchen"></i>
                                                <figcaption>Kitchen</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-bathroom"></i>
                                                <figcaption>Bathroom</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-wardrobe"></i>
                                                <figcaption>Wardrobe</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-shoe-cabinet"></i>
                                                <figcaption>Shoe cabinet</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-office"></i>
                                                <figcaption>Office</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-bar-set"></i>
                                                <figcaption>Bar sets</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-lightning"></i>
                                                <figcaption>Lightning</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="col-sm-3 col-xs-6">
                                        <a href="products-grid.html">
                                            <figure>
                                                <i class="f-icon f-icon-carpet"></i>
                                                <figcaption>Carpet</figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <a href="#">Megamenu <span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                <div class="navbar-dropdown">
                    <div class="navbar-box">
                        <div class="box-2">
                            <div class="box clearfix">
                                <div class="row">
                                    <div class="clearfix">
                                        <div class="col-md-3">
                                            <ul>
                                                <li class="label">Seating</li>
                                                <li><a href="#">Benches</a></li>
                                                <li><a href="#">Submenu</a></li>
                                                <li><a href="#">Chaises</a></li>
                                                <li><a href="#">Recliners</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <ul>
                                                <li class="label">Storage</li>
                                                <li><a href="#">Bockcases</a></li>
                                                <li><a href="#">Closets</a></li>
                                                <li><a href="#">Wardrobes</a></li>
                                                <li><a href="#">Dressers</a></li>
                                                <li><a href="#">Sideboards</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <ul>
                                                <li class="label">Tables</li>
                                                <li><a href="#">Consoles</a></li>
                                                <li><a href="#">Desks</a></li>
                                                <li><a href="#">Dining tables</a></li>
                                                <li><a href="#">Occasional tables</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <ul>
                                                <li class="label">Chairs</li>
                                                <li><a href="#">Dining Chairs</a></li>
                                                <li><a href="#">Office Chairs</a></li>
                                                <li><a href="#">Lounge Chairs</a></li>
                                                <li><a href="#">Stools</a></li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-3">
                                            <ul>
                                                <li class="label">Kitchen</li>
                                                <li><a href="#">Kitchen types</a></li>
                                                <li><a href="#">Kitchen elements</a></li>
                                                <li><a href="#">Bars</a></li>
                                                <li><a href="#">Wall decoration</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <ul>
                                                <li class="label">Accessories</li>
                                                <li><a href="#">Coat Racks</a></li>
                                                <li><a href="#">Lazy bags</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <ul>
                                                <li class="label">Beds</li>
                                                <li><a href="#">Beds</a></li>
                                                <li><a href="#">Sofabeds</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <ul>
                                                <li class="label">Entertainment</li>
                                                <li><a href="#">Wall units</a></li>
                                                <li><a href="#">Media sets</a></li>
                                                <li><a href="#">Decoration</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li><a href="shortcodes.html">Shortcodes</a></li>
        </ul>
    </div>
</div>