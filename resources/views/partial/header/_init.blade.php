<section class="header-content">
    @include('partial.header._slider')
</section>

<!-- ========================  Icons slider ======================== -->
<section class="owl-icons-wrapper owl-icons-frontpage">
    @include('partial.header._icons')
</section>