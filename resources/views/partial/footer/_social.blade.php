<div class="footer-social">
    <div class="row">
        <div class="col-sm-6">
            <a href="#">Sitemap</a> &nbsp; | &nbsp; <a href="#">Privacy policy</a>
        </div>
        <div class="col-sm-6 links">
            <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            </ul>
        </div>
    </div>
</div>