<!--footer showroom-->
@include('partial.footer._showcase')

<!--footer links-->
<div class="footer-links">
    <div class="row">
        <div class="col-sm-4 col-md-2">
            <h5>Browse by</h5>
            <ul>
                <li><a href="#">Brand</a></li>
                <li><a href="#">Product</a></li>
                <li><a href="#">Category</a></li>
            </ul>
        </div>
        <div class="col-sm-4 col-md-2">
            <h5>Recources</h5>
            <ul>
                <li><a href="#">Design</a></li>
                <li><a href="#">Projects</a></li>
                <li><a href="#">Sales</a></li>
            </ul>
        </div>
        <div class="col-sm-4 col-md-2">
            <h5>Our company</h5>
            <ul>
                <li><a href="#">About</a></li>
                <li><a href="#">News</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </div>
        <div class="col-sm-12 col-md-6">
            <h5>Sign up for our newsletter</h5>
            <p><i>Add your email address to sign up for our monthly emails and to receive promotional offers.</i></p>
            <div class="form-group form-newsletter">
                <input class="form-control" type="text" name="email" value="" placeholder="Email address" />
                <input type="submit" class="btn btn-clean btn-sm" value="Subscribe" />
            </div>
        </div>
    </div>
</div>

<!--footer social-->
@include('partial.footer._social')