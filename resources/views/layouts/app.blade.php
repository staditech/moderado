<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Mobile Web-app fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">

    <!-- Meta tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ url('favicon.ico') }}">

    <!--Title-->
    <title>MODERADO DESIGN</title>

    <!--CSS styles-->
    <link rel="stylesheet" media="screen" href="{{ url('css/bootstrap.css') }}" />
    <link rel="stylesheet" media="screen" href="{{ url('css/animate.css') }}" />
    <link rel="stylesheet" media="screen" href="{{ url('css/font-awesome.css') }}" />
    <link rel="stylesheet" media="screen" href="{{ url('css/furniture-icons.css') }}" />
    <link rel="stylesheet" media="screen" href="{{ url('css/linear-icons.css') }}" />
    <link rel="stylesheet" media="screen" href="{{ url('css/magnific-popup.css') }}" />
    <link rel="stylesheet" media="screen" href="{{ url('css/owl.carousel.css') }}" />
    <link rel="stylesheet" media="screen" href="{{ url('css/ion-range-slider.css') }}" />
    <link rel="stylesheet" media="screen" href="{{ url('css/theme.css') }}" />
    <link rel="stylesheet" media="screen" href="{{ url('css/moderado.css') }}" />

    <!--Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div class="page-loader"></div>

<div class="wrapper">

    <!-- ======================== Navigation ======================== -->
    @include('partial.navigation._init')


    <!-- ========================  Header content ======================== -->
    @include('partial.header._init')


    @yield('content')


    <!-- ================== Footer  ================== -->

    <footer>
        <div class="container">
            @include('partial.footer._init')
        </div>
    </footer>

</div> <!--/wrapper-->

<!--JS files-->
<script src="{{ url('js/jquery.min.js') }}"></script>
<script src="{{ url('js/jquery.bootstrap.js') }}"></script>
<script src="{{ url('js/jquery.magnific-popup.js') }}"></script>
<script src="{{ url('js/jquery.owl.carousel.js') }}"></script>
<script src="{{ url('js/jquery.ion.rangeSlider.js') }}"></script>
<script src="{{ url('js/main.js') }}"></script>
</body>

</html>